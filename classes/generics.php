<?php

/**
 * @property int $poids ((En grammes))
 * @property int $hauteur ((En Centimetres))
 * @property string $couleur_principal
 * @property string $couleur_secondaire
 * @property array $etat ((True === Poupee Fermée, False === Poupee Ouverte))
 */

class PoupeeRusse implements Poupee {
    private int $poids;
    private int $hauteur;
    private string $couleur_principal;
    private string $couleur_secondaire;
    private array $poupee_contenu;
    private bool $etat;

    public function __construct($c_poids, $c_hauteur, $c_couleur_principal, $c_couleur_secondaire) {
        $this->poids = $c_poids;
        $this->hauteur = $c_hauteur;
        $this->couleur_principal = $c_couleur_principal;
        $this->couleur_secondaire = $c_couleur_secondaire;
        $this->poupee_contenu = [];
        $this->etat = false;
    }

    public function ouvrir():string{
        if($this->etat) {
            $this->etat = false;
            return "Poupée Ouverte";
        } else {
            return "Deja Ouverte";
        }
    }

    public function fermer():string{
        if(!$this->etat) {
            $this->etat = true;
            return "Poupée Fermée";
        } else {
            return "Deja Fermée";
        }
    }

    public function placerDans(PoupeeRusse $p):string {
        if(!$p->etat){
            if(count($p->poupee_contenu) < 1) {
                if($this->hauteur < $p->hauteur) {
                    if(!$this->etat) {
                        array_push($p->poupee_contenu, $this);
                        return "T'as inséré la poupée de taille " . $this->hauteur . " dans la poupée de " . $p->hauteur;
                    } else {
                        return "Tu dois fermer la petite poupée";
                    }
                } else {
                    return "Tu essayer de placer une poupee trop grande dans une poupee petite";
                }
            } else {
                return "La poupee plus grande contien deja une autre poupee";
            }
        } else {
            return "Tu dois ouvrir la poupee plus grande !!";
        }
    }

    public function sortirDe(PoupeeRusse $p):string {
        if($this === $p->poupee_contenu[0]) {
            $p->poupee_contenu = [];
            return "T'as retirer la poupée de taille " . $this->hauteur . " dans la poupée de " . $p->hauteur;
        } else {
            return "T'as poupée ne contien pas la poupée que t'essayé de retirer!!"; 
        }
    }
}