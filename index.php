<?php

require('./interfaces/generics.php');
require('./classes/generics.php');

$grande = new PoupeeRusse(300, 30, 'Rose', 'Blanc');
$intermediare = new PoupeeRusse(200, 20, 'Rose', 'Blanc');
$petite = new PoupeeRusse(100, 10, 'Rose', 'Blanc');

echo('<pre>');

var_dump($intermediare->ouvrir());
var_dump($petite->placerDans($intermediare));

var_dump($intermediare);

var_dump($intermediare->placerDans($grande));

var_dump($grande);
