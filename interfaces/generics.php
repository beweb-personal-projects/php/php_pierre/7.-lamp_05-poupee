<?php 

interface Poupee {
    public function ouvrir():string;
    public function fermer():string;
    public function placerDans(PoupeeRusse $p):string;
    public function sortirDe(PoupeeRusse $p):string;
}